package com.ita.lardiTest.csv;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import com.googlecode.jcsv.reader.CSVReader;
import com.googlecode.jcsv.reader.internal.CSVReaderBuilder;
import com.googlecode.jcsv.writer.CSVWriter;
import com.googlecode.jcsv.writer.internal.CSVWriterBuilder;
import com.ita.lardiTest.domain.User;
import com.ita.lardiTest.iocustom.readers.CSVReaderApp;
import com.ita.lardiTest.iocustom.writers.CSVWriterApp;

public class CSVTest {
	
	//https://code.google.com/p/jcsv/wiki/CSVEntryParserExample
	@Test
	public void testRead() throws IOException{
		File file = new File("D:\\java\\test\\users.csv");
		if(!file.exists()){
			file.createNewFile();//check for main work of app
		}
		Reader reader = new FileReader("D:\\java\\test\\users.csv");
		CSVReader<User> csvReader = new CSVReaderBuilder<User>(reader).entryParser(new CSVReaderApp()).build();
		List<User> users = csvReader.readAll();
		reader.close();
		Iterator<User> it = users.iterator();
		while(it.hasNext()){
			User u = it.next();
			System.out.println(u.toString());//debug
		}
	}
	
	@Test
	public void testWrite() throws IOException {
		List<User> list = new ArrayList<User>();
		User user = new User();
		user.setId(1);
		user.setFirstName("aa");
		user.setMobilePhone(23542352);
		user.setSeckondName("srfqwrsd");
		user.setPatronymic("afawf");
		list.add(user);
		/*User user2 = new User();
		user2.setId(2);
		user2.setFirstName("awffaa");
		user2.setSeckondName("rawrawra");
		user2.setHomePhone(32525235);
		user2.setPatronymic("afawf");
		user2.setMobilePhone(236262512);
		list.add(user2);*/
		File file = new File("D:\\java\\test\\users.csv");	
		Writer out = new FileWriter(file);
		CSVWriter<User> csvWriter = new CSVWriterBuilder<User>(out).entryConverter(new CSVWriterApp()).build();
		csvWriter.writeAll(list);
		out.flush();
		out.close();
	}
}
