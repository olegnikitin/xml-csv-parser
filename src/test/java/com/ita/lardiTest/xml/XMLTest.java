package com.ita.lardiTest.xml;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.ita.lardiTest.domain.Group;
import com.ita.lardiTest.domain.User;
import com.ita.lardiTest.iocustom.readers.XMLReaderApp;
import com.ita.lardiTest.iocustom.writers.XMLWriterApp;

public class XMLTest {	
	
	@Test
	public void writeToXML() {
		XMLWriterApp xmlWriterApp = new XMLWriterApp();
		Group group = new Group();
		User user = new User();
		user.setId(1);
		user.setFirstName("wyryt");
		user.setSeckondName("ujdujduj");
		user.setPatronymic("yuddiu");
		user.setMobilePhone(236463261);
		User user2 = new User();//only second file will be written. the reason is: http://dev64.wordpress.com/2012/05/15/using-annotations-with-jaxb/
		user2.setFirstName("second");
		List<User> users = new ArrayList<>();
		users.add(user2);
		users.add(user);
		group.setMembers(users);
		xmlWriterApp.objectToXML(group);
	}
	
	@Test
	public void readFromXML() {		
		XMLReaderApp xmlReaderApp = new XMLReaderApp();
		Group group = xmlReaderApp.xmlToObject();
		System.out.println(group.toString());
	}
}
