package com.ita.lardiTest.service;

import java.util.Scanner;

import com.ita.lardiTest.domain.User;

public interface UserService {
	public void listOfUsers();
	public User getUserById(Integer id);
	public void createUser(Scanner sc);
	public void editUser(Integer id, Scanner sc);
	public void deleteUser(Integer id);
}
