package com.ita.lardiTest.service;

import java.util.Iterator;
import java.util.Scanner;

import com.ita.lardiTest.dao.UserDAO;
import com.ita.lardiTest.dao.UserDAOImpl;
import com.ita.lardiTest.domain.User;

public class UserServiceImpl implements UserService {

	private UserDAO userDao = new UserDAOImpl();

	@Override
	public void listOfUsers() {
		if(!userDao.listOfUsers().isEmpty()){
			Iterator<User> it = userDao.listOfUsers().iterator();
			while (it.hasNext()) {
				User u = it.next();
				System.out.println(u.toString());
			}
		}else{
			System.out.println("The list is empty");
		}		
	}

	@Override
	public User getUserById(Integer id) {
		return userDao.getUserById(id);
	}

	@Override
	public void createUser(Scanner sc) {
		User user = new User();
		
		System.out.println("Enter your seckondName");
		String seckondName = sc.nextLine();
		System.out.println("Enter your firstName");
		String firstName = sc.nextLine();
		System.out.println("Enter your patronymic");
		String patronymic = sc.nextLine();
		System.out.println("Enter your mobilePhone");
		Integer mobilePhone = Integer.parseInt(sc.nextLine());
		if (seckondName.equals("") || firstName.equals("") || patronymic.equals("") || mobilePhone.equals("")) {
			System.out.println("You should enter your seckondName, firstName, patronymic and mobilePhone");
			createUser(sc);
		}

		System.out.println("Enter your homePhone");
		Integer homePhone = Integer.parseInt(sc.nextLine());
		System.out.println("Enter your address");
		String address = sc.nextLine();
		System.out.println("Enter your e_mail");
		String e_mail = sc.nextLine();

		user.setId((userDao.listOfUsers().size())+1);
		user.setSeckondName(seckondName);
		user.setFirstName(firstName);
		user.setPatronymic(patronymic);
		user.setMobilePhone(mobilePhone);
		user.setHomePhone(homePhone);
		user.setAddress(address);
		user.setE_mail(e_mail);		
		
		//System.out.println(user);
		userDao.createUser(user);
	}

	@Override
	public void editUser(Integer id, Scanner sc) {
		User user = this.getUserById(id);
		System.out.println(user.toString());
		
		System.out.println("Enter your seckondName");
		String seckondName = sc.nextLine();
		System.out.println("Enter your firstName");
		String firstName = sc.nextLine();
		System.out.println("Enter your patronymic");
		String patronymic = sc.nextLine();
		System.out.println("Enter your mobilePhone");
		Integer mobilePhone = Integer.parseInt(sc.nextLine());
		if (seckondName.equals("") || firstName.equals("") || patronymic.equals("") || mobilePhone.equals("")) {
			System.out.println("You should enter your seckondName, firstName, patronymic and mobilePhone");
			createUser(sc);
		}

		System.out.println("Enter your homePhone");
		Integer homePhone = Integer.parseInt(sc.nextLine());
		System.out.println("Enter your address");
		String address = sc.nextLine();
		System.out.println("Enter your e_mail");
		String e_mail = sc.nextLine();

		user.setSeckondName(seckondName);
		user.setFirstName(firstName);
		user.setPatronymic(patronymic);
		user.setMobilePhone(mobilePhone);
		user.setHomePhone(homePhone);
		user.setAddress(address);
		user.setE_mail(e_mail);
		
		userDao.editUser(user);
	}

	@Override
	public void deleteUser(Integer id) {
		userDao.deleteUser(id);
	}
}
