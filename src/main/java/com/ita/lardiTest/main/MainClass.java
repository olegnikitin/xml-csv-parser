package com.ita.lardiTest.main;

import java.util.Scanner;

import com.ita.lardiTest.main.Configuration.CONFIG;
import com.ita.lardiTest.service.UserService;
import com.ita.lardiTest.service.UserServiceImpl;

public class MainClass {
	
	private UserService userService = new UserServiceImpl();
	
	private Scanner sc = new Scanner(System.in);
	
	public MainClass() {
		firstMenu();
	}
	
	private void exit()
	{
		System.out.println("You've exited the program");
		sc.close();
		System.exit(0);
	}
	
	private void firstMenu(){
		System.out.println("Choose the way to show elements:");
		System.out.println("1 - exit\n2 - show in XML\n3 - show in CSV");
		int style = 0;
		if(sc.hasNextInt()) 
		{
			style = Integer.parseInt(sc.nextLine());			
		}else System.out.println("Enter the number 1-3");
		switch (style) {
		case 1: exit(); break;
		case 2: Configuration.config = CONFIG.XML; seckondMenu(); break;
		case 3: Configuration.config = CONFIG.CSV; seckondMenu(); break;
		default:
			System.out.println("shouldn't happened");//debug
			firstMenu();
		}
	}

	private void seckondMenu(){
		System.out.println("1 - exit\n2 - see all records\n3 - add a record\n4 - edit a record\n5 - delete a record");
		int style = 0;
		if(sc.hasNextInt()) 
		{
			style = Integer.parseInt(sc.nextLine());			
		}else System.out.println("Enter the number 1-5");
		switch (style) {
		case 1: exit(); break;
		case 2: userService.listOfUsers(); seckondMenu(); break;
		case 3: userService.createUser(sc); seckondMenu(); break;
		case 4: userService.editUser(Integer.parseInt(sc.nextLine()), sc); break;
		case 5: userService.deleteUser(Integer.parseInt(sc.nextLine())); seckondMenu(); break;
		
		default:
			System.out.println("shouldn't happened");//debug
			seckondMenu();
		}
	}
	
	public static void main(String[] args) {
		new MainClass();
	}
}
