package com.ita.lardiTest.dao;

import java.util.List;

import com.ita.lardiTest.domain.User;

public interface UserDAO {
	public List<User> listOfUsers();
	public User getUserById(Integer id);
	public void createUser(User user);
	public void editUser(User user);
	public void deleteUser(Integer id);
}
