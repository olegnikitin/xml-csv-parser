package com.ita.lardiTest.dao;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.List;

import com.googlecode.jcsv.reader.CSVReader;
import com.googlecode.jcsv.reader.internal.CSVReaderBuilder;
import com.googlecode.jcsv.writer.CSVWriter;
import com.googlecode.jcsv.writer.internal.CSVWriterBuilder;
import com.ita.lardiTest.domain.Group;
import com.ita.lardiTest.domain.User;
import com.ita.lardiTest.iocustom.readers.CSVReaderApp;
import com.ita.lardiTest.iocustom.readers.XMLReaderApp;
import com.ita.lardiTest.iocustom.writers.CSVWriterApp;
import com.ita.lardiTest.iocustom.writers.XMLWriterApp;
import com.ita.lardiTest.main.Configuration;

public class UserDAOImpl implements UserDAO {

	private final File fileCSV = new File("D:\\java\\test\\users.csv");
	private final File fileXML = new File("D:\\java\\test\\storage.xml");

	@Override
	public List<User> listOfUsers() {
		List<User> users = null;
		try {
			if (!fileCSV.exists()) {
				System.out.println("it's a first start of program");
				fileCSV.createNewFile();
			}
			if (Configuration.config == Configuration.CONFIG.CSV) {
				Reader reader = new FileReader(fileCSV);
				CSVReader<User> csvReader = new CSVReaderBuilder<User>(reader)
						.entryParser(new CSVReaderApp()).build();
				users = csvReader.readAll();
				reader.close();
			} else if (Configuration.config == Configuration.CONFIG.XML) {
				XMLReaderApp xmlReaderApp = new XMLReaderApp();
				Group group = xmlReaderApp.xmlToObject();
				users = group.getMembers();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return users;
	}

	@Override
	public User getUserById(Integer id) {
		User user = listOfUsers().get(id);
		return user;
	}

	@Override
	public void createUser(User user) {
		try {
			if (Configuration.config == Configuration.CONFIG.CSV) {// something
																	// is wrong
																	// with this
																	// method
				Writer out = new FileWriter(fileCSV);
				CSVWriter<User> csvWriter = new CSVWriterBuilder<User>(out)
						.entryConverter(new CSVWriterApp()).build();
				List<User> list = this.listOfUsers();
				list.add(user);
				csvWriter.writeAll(list);
				out.flush();
				out.close();
			} else if (Configuration.config == Configuration.CONFIG.XML) {
				XMLWriterApp xmlWriterApp = new XMLWriterApp();
				Group group = new Group();
				List<User> users = this.listOfUsers();
				users.add(user);
				group.setMembers(users);
				xmlWriterApp.objectToXML(group);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void editUser(User user) {
		try {
			if (Configuration.config == Configuration.CONFIG.CSV) {// TODO:
																	// write it
				Writer out = new FileWriter(fileCSV);
				CSVWriter<User> csvWriter = new CSVWriterBuilder<User>(out)
						.entryConverter(new CSVWriterApp()).build();
				List<User> list = this.listOfUsers();
				fileCSV.delete();
				fileCSV.createNewFile();
				Integer number = user.getId();
				list.remove(number);
				list.add(number, user);
				csvWriter.writeAll(list);
				out.flush();
				out.close();
			} else if (Configuration.config == Configuration.CONFIG.XML) {
				XMLWriterApp xmlWriterApp = new XMLWriterApp();
				Group group = new Group();
				List<User> users = this.listOfUsers();
				fileXML.delete();
				fileXML.createNewFile();
				Integer number = user.getId();
				users.remove(number);
				users.add(number, user);
				xmlWriterApp.objectToXML(group);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteUser(Integer id) {
		List<User> users = listOfUsers();
		users.remove(id);
		try {
			if (Configuration.config == Configuration.CONFIG.CSV) {
				fileCSV.delete();
				fileCSV.createNewFile();

				Writer out = new FileWriter(fileCSV);
				CSVWriter<User> csvWriter = new CSVWriterBuilder<User>(out)
						.entryConverter(new CSVWriterApp()).build();
				csvWriter.writeAll(users);
				out.flush();
				out.close();
			} else if (Configuration.config == Configuration.CONFIG.XML) {
				XMLWriterApp xmlWriterApp = new XMLWriterApp();
				Group group = new Group();
				group.setMembers(users);
				fileXML.delete();
				fileXML.createNewFile();
				
				xmlWriterApp.objectToXML(group);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
