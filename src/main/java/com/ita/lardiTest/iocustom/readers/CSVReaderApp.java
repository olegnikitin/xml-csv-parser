package com.ita.lardiTest.iocustom.readers;

import com.googlecode.jcsv.reader.CSVEntryParser;
import com.ita.lardiTest.domain.User;

public class CSVReaderApp implements CSVEntryParser<User> {
	
	@Override
	public User parseEntry(String... data) {
		
		User user = new User();
		
		//creating variables in case of increasing readability of code
		Integer id = Integer.parseInt(data[0]);
		String seckondName = data[1];
		String firstName = data[2];
		String patronymic = data[3];
		Integer mobilePhone = Integer.parseInt(data[4]);
		
		//emptyable variables
		Integer homePhone;
		if(data[5].equals("")){
			homePhone = null;
		}else {
			homePhone = Integer.parseInt(data[5]);
		}
		String address;
		if(data[6].equals("")){
			address = null;
		}else {
			address = data[6];
		}		
		String e_mail;
		if(data[7].equals("")){
			e_mail = null;
		}else {
			e_mail = data[7];
		}
		
		user.setId(id);
		user.setSeckondName(seckondName);
		user.setFirstName(firstName);
		user.setPatronymic(patronymic);
		user.setMobilePhone(mobilePhone);
		user.setHomePhone(homePhone);
		user.setAddress(address);
		user.setE_mail(e_mail);
		
		return user;
	}

}
