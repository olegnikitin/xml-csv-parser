package com.ita.lardiTest.iocustom.readers;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.ita.lardiTest.domain.Group;

public class XMLReaderApp{
	
	private final File file = new File("D:\\java\\test\\storage.xml");
	
	public Group xmlToObject(){
		if(!file.exists()){
			System.out.println("list of users is empty");
		}else{
			try {			
	            JAXBContext context = JAXBContext.newInstance(Group.class);
	            Unmarshaller un = context.createUnmarshaller();
	            Group group = (Group) un.unmarshal(file);
	            return group;
	        } catch (JAXBException e) {
	            e.printStackTrace();
	        }
		}
        return null;
	}
}
