package com.ita.lardiTest.iocustom.writers;

import com.googlecode.jcsv.writer.CSVEntryConverter;
import com.ita.lardiTest.domain.User;

public class CSVWriterApp implements CSVEntryConverter<User> {

	@Override
	public String[] convertEntry(User user) {
		String[] columns = new String[8];
		String empty = "";
		
		columns[0] = user.getId()+"";
		columns[1] = user.getSeckondName();
		columns[2] = user.getFirstName();
		columns[3] = user.getPatronymic();
		columns[4] = user.getMobilePhone()+"";
		if(user.getHomePhone() == null){
			columns[5] = empty;
		}
		if(user.getAddress() == null){
			columns[6] = empty;
		}
		if(user.getE_mail() == null){
			columns[7] = empty;
		}
		
		return columns;
	}
	
}
