package com.ita.lardiTest.iocustom.writers;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.ita.lardiTest.domain.Group;
//http://www.journaldev.com/1234/jaxb-tutorial-example-to-convert-object-to-xml-and-xml-to-object
public class XMLWriterApp {
	
	private final File file = new File("D:\\java\\test\\storage.xml");
	
	public void objectToXML(Group group){		
		try {
			if(!file.exists()){
				file.createNewFile();
			}
            JAXBContext context = JAXBContext.newInstance(Group.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); 
            // Write to System.out for debugging
            // m.marshal(emp, System.out);
            m.marshal(group, file);
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (IOException e) {
			e.printStackTrace();
		}
	}
}
