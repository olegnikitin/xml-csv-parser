package com.ita.lardiTest.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlType;

/*
 * Real user
 */
@XmlType(propOrder = {"id", "seckondName", "firstName", "patronymic", "mobilePhone", "homePhone", "address", "e_mail"}, name = "user")
public class User implements Serializable{
	
	private static final long serialVersionUID = 76987546767L;

	private Integer id;
	
	//methods that must be present
	private String seckondName, firstName, patronymic;
	private Integer mobilePhone;
	
	//methods that can be present
	private Integer homePhone;
	private String address, e_mail;	
	
	public User() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSeckondName() {
		return seckondName;
	}

	public void setSeckondName(String seckondName) {
		this.seckondName = seckondName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPatronymic() {
		return patronymic;
	}

	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}

	public Integer getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(Integer mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public Integer getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(Integer homePhone) {
		this.homePhone = homePhone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getE_mail() {
		return e_mail;
	}

	public void setE_mail(String e_mail) {
		this.e_mail = e_mail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((mobilePhone == null) ? 0 : mobilePhone.hashCode());
		result = prime * result
				+ ((patronymic == null) ? 0 : patronymic.hashCode());
		result = prime * result
				+ ((seckondName == null) ? 0 : seckondName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (mobilePhone == null) {
			if (other.mobilePhone != null)
				return false;
		} else if (!mobilePhone.equals(other.mobilePhone))
			return false;
		if (patronymic == null) {
			if (other.patronymic != null)
				return false;
		} else if (!patronymic.equals(other.patronymic))
			return false;
		if (seckondName == null) {
			if (other.seckondName != null)
				return false;
		} else if (!seckondName.equals(other.seckondName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", seckondName=" + seckondName
				+ ", firstName=" + firstName + ", patronymic=" + patronymic
				+ ", mobilePhone=" + mobilePhone + ", homePhone=" + homePhone
				+ ", address=" + address + ", e_mail=" + e_mail + "]";
	}
}