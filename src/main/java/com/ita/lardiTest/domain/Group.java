package com.ita.lardiTest.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Олег
 * the usage of class is xml mrsh & unmrsh
 */
@XmlRootElement(name = "group")
public class Group {
    private List<User> members = new ArrayList<User>();
    
	@XmlElementWrapper(name = "users")
	@XmlElement(name = "user")
	public List<User> getMembers() {
		return members;
	}
	public void setMembers(List<User> members) {
		this.members = members;
	}
	@Override
	public String toString() {
		return "Group [members=" + members + "]";
	}
}
